#/usr/bin/env bash

_get_env_name()
{
    if [[ ${COMP_WORDS[0]} =~ "-dev" ]]; then
        /usr/sbin/kaenv3-dev --list --json | jq -r .[].name | sort | uniq
    else
        kaenv3 --list --json | jq -r .[].name | sort | uniq
    fi
}

_get_env_arch()
{
    if [[ ${COMP_WORDS[0]} =~ "-dev" ]]; then
        /usr/sbin/kaenv3-dev --list --json | jq -r .[].arch | sort | uniq
    else
        kaenv3 --list --json | jq -r .[].arch | sort | uniq
    fi
}

_get_oar_node()
{
    if [[ ! -z "${OAR_NODE_FILE}" ]] && [[ -f "${OAR_NODE_FILE}" ]];then
        cat "${OAR_NODE_FILE}"  | sort | uniq
    fi
}

_get_oar_env_variable()
{
    if [[ ! -z "${OAR_NODE_FILE}" ]];then
        echo '$OAR_NODE_FILE'
    fi
}


_kadeploy()
{
    local cur prev fileops OPTS
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    case $prev in
        '-u'|'--env-user')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -u -- $cur) )
            return 0
            ;;
        '-e'|'--env-name')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -W '$(_get_env_name)' -- $cur) )
            return 0
            ;;
        '-A'|'--env-arch')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -W '$(_get_env_arch)' -- $cur) )
            return 0
            ;;
        '-m'|'--machine')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -W '$(_get_oar_node)' -- $cur) )
            return 0
            ;;
        '-f'|'--file')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -f -W '$(_get_oar_env_variable)' -- $cur) )
            return 0
            ;;
        '-n'|'--output-ko-nodes'|'-o'|'--output-ok-nodes'|'-s'|'--script'|'--write-workflow-id'|'--custom-steps'|'-a'|'--env-file'|'-k'|'--key'|'-w'|'--set-pxe-profile'|'--set-pxe-pattern'|'--upload-pxe-files')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -f -- $cur) )
            return 0
            ;;
    esac
    case $cur in
        -*)
            OPTS="-h
                --help
                -v
                --version
                -I
                --server-info
                -M
                --multi-server
                -H
                --[no-]debug-http
                -S
                --server
                --dry-run
                --no-dry-run
                --password
                -d
                --debug-mode
                --no-debug-mode
                -f
                --file
                -m
                --machine
                -n
                --output-ko-nodes
                -o
                --output-ok-nodes
                -s
                --script
                -V
                --verbose-level
                --write-workflow-id
                --wait
                --no-wait
                --force
                --no-force
                --breakpoint
                --custom-steps
                --no-hook
                --hook
                -a
                --env-file
                -b
                --block-device
                -c
                --boot-partition
                -e
                --env-name
                -k
                --key
                -p
                --partition-label
                -r
                --reformat-tmp
                -u
                --env-user
                --vlan
                -w
                --set-pxe-profile
                --set-pxe-pattern
                -x
                --upload-pxe-files
                --env-version
                -A
                --env-arch
                --no-kexec
                --force-untrust-current-env
                --force-trust-current-env
                --disable-bootloader-install
                --disable-disk-partitioning
                --reboot-classical-timeout
                --reboot-kexec-timeout
                --force-steps
                --secure
                --no-secure"
            COMPREPLY=( $(compgen -W "${OPTS[*]}" -- $cur) )
            return 0
            ;;
    esac
    return 0
}


_kaenv()
{
    local cur prev fileops OPTS
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    case $prev in
        '-u'|'--env-user')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -u -- $cur) )
            return 0
            ;;
        '-e'|'--env-name'|'-p'|'--print'|'-d'|'--delete'|'--toggle-destructive-tag'|'--set-visibility-tag'|'--set-arch'|'--update-image-checksum'|'--update-preinstall-checksum'|'--update-postinstalls-checksum')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -W '$(_get_env_name)' -- $cur) )
            return 0
            ;;
        '-A'|'--env-arch','--new-arch')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -W '$(_get_env_arch)' -- $cur) )
            return 0
            ;;
        '-a'|'--env-file'|'-m'|'--file-to-move')
            local IFS=$'\n'
            COMPREPLY=( $(compgen -f -- $cur) )
            return 0
            ;;
    esac
    case $cur in
        -*)
            OPTS="-h
                --help
                -v
                --version
                -I
                --server-info
                -M
                --multi-server
                -H
                --[no-]debug-http
                -S
                --server
                --dry-run
                --no-dry-run
                --password
                -l
                --list
                --summary
                -p
                --print
                -a
                --add
                -d
                --delete
                --json
                -s
                --al-version
                -u
                --env-user
                --env-version
                -A
                --env-arch
                --yes
                --toggle-destructive-tag
                --set-visibility-tag ENVNAME
                -t
                --visibility-tag TAG
                --set-arch
                --new-arch
                --update-image-checksum
                --update-preinstall-checksum
                --update-postinstalls-checksum
                --move-files
                -m
                --files-to-move
                --secure
                --no-secure"
            COMPREPLY=( $(compgen -W "${OPTS[*]}" -- $cur) )
            return 0
            ;;
    esac
    return 0
}

complete -F _kadeploy kadeploy3-dev
complete -F _kaenv kaenv3-dev
