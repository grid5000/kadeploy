module Kadeploy

module Macrostep

########################
### SetDeploymentMiniOS ###
########################

  class DeploySetDeploymentMiniOS < Deploy
  end

  class DeploySetDeploymentMiniOSUntrusted < DeploySetDeploymentMiniOS
    def steps()
      [
        [:switch_pxe, "prod_to_deploy_env", ""],
        [:set_default_vlan],
        [:reboot, "soft"],
        [:wait_reboot],
        [:startup_script],
        [:send_key_in_deploy_env, :tree],
        [:create_partition_table],
        [:format_deploy_part],
        [:mount_deploy_part],
        [:format_tmp_part],
        [:format_swap_part],
      ]
    end
  end

  class DeploySetDeploymentMiniOSKexec < DeploySetDeploymentMiniOS
    def steps()
      [
        [:switch_pxe, "prod_to_deploy_env", ""],
        [:send_deployment_kernel, :tree],
        [:set_default_vlan],
        [:kexec, :to_deploy_kernel],
        [:wait_reboot],
        [:startup_script],
        [:send_key_in_deploy_env, :tree],
        [:create_partition_table],
        [:format_deploy_part],
        [:mount_deploy_part],
        [:format_tmp_part],
        [:format_swap_part],
      ]
    end
  end

  # Same as DeploySetDeploymentMiniOSKexec, but we override to Untrused if the last
  # env is not trusted in runtime
  class DeploySetDeploymentMiniOSTrusted < DeploySetDeploymentMiniOSKexec
  end

  class DeploySetDeploymentMiniOSUntrustedCustomPreInstall < DeploySetDeploymentMiniOS
    def steps()
      [
        [:switch_pxe, "prod_to_deploy_env"],
        [:set_default_vlan],
        [:reboot, "soft"],
        [:wait_reboot],
        [:startup_script],
        [:send_key_in_deploy_env, :tree],
        [:manage_admin_pre_install, :tree],
      ]
    end
  end

  class DeploySetDeploymentMiniOSProd < DeploySetDeploymentMiniOS
    def steps()
      [
        [:format_deploy_part],
        [:mount_deploy_part],
        [:format_tmp_part],
      ]
    end
  end

  class DeploySetDeploymentMiniOSNfsroot < DeploySetDeploymentMiniOS
    def steps()
      [
        [:switch_pxe, "prod_to_nfsroot_env"],
        [:set_default_vlan],
        [:reboot, "soft"],
        [:wait_reboot],
        [:startup_script],
        [:send_key_in_deploy_env, :tree],
        [:create_partition_table],
        [:format_deploy_part],
        [:mount_deploy_part],
        [:format_tmp_part],
        [:format_swap_part],
      ]
    end
  end

  class DeploySetDeploymentMiniOSDummy < DeploySetDeploymentMiniOS
    def steps()
      [
        [:dummy],
        [:dummy],
      ]
    end
  end


####################
### BroadcastEnv ###
####################

  class DeployBroadcastEnv < Deploy
  end

  class DeployBroadcastEnvChain < DeployBroadcastEnv
    def steps()
      [
        [:send_environment, :chain],
        [:decompress_environment, :tree],
        [:manage_admin_post_install, :tree],
        [:manage_user_post_install, :tree],
        [:check_kernel_files],
        [:send_key, :tree],
        [:sync],
        [:install_bootloader],
      ]
    end
  end

  class DeployBroadcastEnvKascade < DeployBroadcastEnv
    def steps()
      [
        [:send_environment, :kascade],
        [:decompress_environment, :tree],
        [:mount_deploy_part],
        [:manage_admin_post_install, :tree],
        [:manage_user_post_install, :tree],
        [:check_kernel_files],
        [:send_key, :tree],
        [:sync],
        [:install_bootloader],
      ]
    end
  end

  class DeployBroadcastEnvKastafior < DeployBroadcastEnv
    def steps()
      [
        [:send_environment, :kastafior],
        [:decompress_environment, :tree],
        [:mount_deploy_part],
        [:manage_admin_post_install, :tree],
        [:manage_user_post_install, :tree],
        [:check_kernel_files],
        [:send_key, :tree],
        [:sync],
        [:install_bootloader],
      ]
    end
  end

  class DeployBroadcastEnvTree < DeployBroadcastEnv
    def steps()
      [
        [:send_environment, :tree],
        [:decompress_environment, :tree],
        [:manage_admin_post_install, :tree],
        [:manage_user_post_install, :tree],
        [:check_kernel_files],
        [:send_key, :tree],
        [:sync],
        [:install_bootloader],
      ]
    end
  end

  class DeployBroadcastEnvBittorrent < DeployBroadcastEnv
    def steps()
      [
        [:mount_tmp_part], # we need /tmp to store the tarball
        [:send_environment, :bittorrent],
        [:decompress_environment, :tree],
        [:manage_admin_post_install, :tree],
        [:manage_user_post_install, :tree],
        [:check_kernel_files],
        [:send_key, :tree],
        [:sync],
        [:install_bootloader],
      ]
    end
  end

  class DeployBroadcastEnvCustom < DeployBroadcastEnv
    def steps()
      [
        [:send_environment, :custom],
        [:decompress_environment, :tree],
        [:manage_admin_post_install, :tree],
        [:manage_user_post_install, :tree],
        [:check_kernel_files],
        [:send_key, :tree],
        [:sync],
        [:install_bootloader],
      ]
    end
  end

  class DeployBroadcastEnvDummy < DeployBroadcastEnv
    def steps()
      [
        [:dummy],
        [:dummy],
      ]
    end
  end


##################
### BootNewEnv ###
##################

  class DeployBootNewEnv < Deploy
  end

  class DeployBootNewEnvKexec < DeployBootNewEnv
    def steps()
      [
        [:switch_pxe, "deploy_to_deployed_env"],
        [:umount_deploy_part],
        [:mount_deploy_part],
        [:kexec, :to_deployed_env],
        [:set_vlan],
        [:wait_reboot, "kexec", "user", true, nil, nil, nil, true],
      ]
    end
  end

  class DeployBootNewEnvPivotRoot < DeployBootNewEnv
    def start!
      debug(0, "#{self.class.name} is not yet implemented")
      kill()
      return false
    end
  end

  class DeployBootNewEnvClassical < DeployBootNewEnv
    DESC = "Reboot to the deployed environment without kexec. Used when a cluster does not support kexec."
    def steps()
      [
        [:switch_pxe, "deploy_to_deployed_env"],
        [:umount_deploy_part],
        [:reboot_from_deploy_env],
        [:set_vlan],
        [:wait_reboot, "classical", "user", true, nil, nil, nil, true],
      ]
    end
  end

  class DeployBootNewEnvHardReboot < DeployBootNewEnv
    def steps()
      [
        [:switch_pxe, "deploy_to_deployed_env"],
        [:reboot, "hard"],
        [:set_vlan],
        [:wait_reboot, "classical", "user", true, nil, nil, nil, true],
      ]
    end
  end

  class DeployBootNewEnvDummy < DeployBootNewEnv
    def steps()
      [
        [:dummy],
        [:dummy],
      ]
    end
  end

end

end
