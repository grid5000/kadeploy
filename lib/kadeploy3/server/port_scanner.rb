require 'socket'

module Kadeploy

module PortScanner
  # Test if a port is open
  #
  # Arguments
  # * hostname: hostname
  # * port: port
  # Output
  # * return true if the port is open, false otherwise
  def PortScanner::is_open?(hostname, port)
    res = false
    tid = Thread.new {
      begin
        s = TCPSocket.new(hostname, port)
        s.close
        res = true
      rescue
        res = false
      end
    }
    start = Time.now.to_i
    while ((tid.status != false) && (Time.now.to_i < (start + 10)))
      sleep(0.05)
    end
    if (tid.status != false) then
      Thread.kill(tid)
      return false
    else
      return res
    end
  end

  # Test if a node accept or refuse connections on every ports of a list (TCP)
  def self.ports_test(nodeid, ports, accept = true)
    max_retry = 2
    current_retry = 0
    ret = true
    ports.each do |port|
      begin
        Timeout.timeout(5) do
          s = TCPSocket.open(nodeid, port)
          s.close
        end
        unless accept
          ret = false
          break
        end
      rescue Errno::ECONNREFUSED
        if accept
          ret = false
          break
        end
      rescue Errno::EHOSTUNREACH
        ret = false
        break
      rescue Timeout::Error
        # We want to retry if we encounter a timeout
        if current_retry >= max_retry
          ret = false if accept
          break
        end
        current_retry += 1
        sleep(1)
        redo
      end
    end
    ret
  end

  def self.ping(hostname, timeout, port)
    ret = true
    begin
      Timeout.timeout(timeout) do
        s = TCPSocket.new(hostname, port)
        s.close
      end
    rescue Errno::ECONNREFUSED
      # do nothing
    rescue Timeout::Error
      ret = false
    rescue StandardError
      ret = false
    end
    ret
  end
end

end
