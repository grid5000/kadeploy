read action
vm=$(VBoxManage list vms | sed "s/\"\(.*\)\".*$/\1/g" | grep vagrant-env.knode-1)



echo "$action on $vm" > /dev/stderr
case "$action" in
  poweroff)
    VBoxManage controlvm $vm  acpipowerbutton 1>&2
    ;;
  hardpoweroff)
    VBoxManage controlvm $vm  poweroff 1>&2
    ;;
  poweron)
    VBoxManage startvm $vm 1>&2
    ;;
  restart)
    VBoxManage controlvm $vm poweroff 1>&2
    sleep 3
    VBoxManage startvm $vm 1>&2
    ;;
  *)
    echo "Operation not known"
    ;;
esac
