---
name: debian9-x64-min-zstd
version: 2020090209
description: debian 9 (stretch) - min on zstd
author: support-staff@lists.grid5000.fr
visibility: public
destructive: false
os: linux
image:
  file: server:///vagrant/envs/debian9-x64-min-2020090209.zstd
  kind: tar
  compression: zstd
postinstalls:
- archive: server:///grid5000/postinstalls/g5k-postinstall.tgz
  compression: gzip
  script: "g5k-postinstall --net debian --net traditional-names -d --no-ref-api"
boot:
  kernel: "/vmlinuz"
  initrd: "/initrd.img"
filesystem: ext4
partition_type: 131
multipart: false
