Vagrant
=======

On your host you need:
* To be inside g5k vpn (to get deb from packages.grid5000.fr)
* To get an environment from g5k
```
* rsync nancy.g5kadmin:/grid5000/images/debian11-x64-min-2024031410.tar.zst envs/debian11-x64-min.tar.zst
```
* To start ncat to allow the kadeploy from vagrant to reboot a virtual machine.
You will need ncat from nmap. (apt install ncat)
* In a separate terminal:
```
  cd vagrant-env
  ncat -k -l --allow localhost --sh-exec $(readlink -f vbhelper.sh)
```
* vagrant up kadeploy
* vagrant up knode-1 #Fail to connect ssh is expected
* vagrant ssh kadeploy
* You may need to start ident2 on kadeploy (`sudo ident2`)
```
vagrant@kadeploy> kadeploy3d -d
vagrant@kadeploy> karights3 -o -a -m node-1 -u vagrant
vagrant@kadeploy> kaenv3 -a /vagrant/envs/debian11-x64-min.dsc
vagrant@kadeploy> kadeploy3 -m node-1 -e debian11-x64-min -V5 -r ext4
```

How to debug
============

Server side using byebug
------------------------

* Add `require byebug` and `byebug` to the line you whish to debug
* start the deamon in foreground without any std fd redirection: `kadeploy3d -IOE`
