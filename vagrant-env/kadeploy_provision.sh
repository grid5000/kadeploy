#!/bin/bash
set -xe

install_package() {
# The following requires to be inside the Grid'5000 network, for instance by using the Grid'5000 VPN
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y gnupg 
wget http://packages.grid5000.fr/grid5000-archive-key.asc -q -O- | apt-key add -
echo deb http://packages.grid5000.fr/deb/g5k-postinstall/ ./ > /etc/apt/sources.list.d/postinstall.list
echo deb http://packages.grid5000.fr/deb/kadeploy-kernels/ ./ > /etc/apt/sources.list.d/kadeploykernel.list

#sed -i '/backports/s|deb.debian.org|archive.debian.org|g' /etc/apt/sources.list
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential ruby rubygems lsb-release git taktuk rake help2man mariadb-server libmariadb-dev ruby-dev ident2 syslinux ncat dnsmasq isc-dhcp-server tftpd-hpa pxelinux apache2 
#DEBIAN_FRONTEND=noninteractive apt-get install -y g5k-postinstall kadeploy3-deploy-kernel-{buster,bullseye}{,-arm64,-ppc64}
DEBIAN_FRONTEND=noninteractive apt-get install -y g5k-postinstall=1.2022042109 kadeploy3-deploy-kernel-{buster,bullseye,bookworm}{,-arm64}
# Optional:
DEBIAN_FRONTEND=noninteractive apt-get install -y vim tcpdump
gem install --conservative --no-document mysql2   
gem install --conservative --no-document net-ssh -v 4.2.0
gem install --conservative --no-document byebug -v 9.0.6
gem install --conservative --no-document rspec
}

setup_tftp() {
mkdir -p /srv/tftp/{pxelinux.cfg,userfiles,kernels}
chmod -R 777 /srv/tftp
cp /vagrant/other_conf/tftpd-hpa /etc/default/tftpd-hpa
cp /usr/lib/PXELINUX/pxelinux.0 /srv/tftp/
cp /usr/lib/PXELINUX/lpxelinux.0 /srv/tftp/
cp /usr/lib/syslinux/modules/bios/ldlinux.c32 /srv/tftp/
cp /grid5000/kadeploy-kernels/kadeploy3-deploy-kernel-*.{vmlinuz,initrd.img} /srv/tftp/kernels
cp /usr/lib/syslinux/modules/bios/ldlinux.c32 /srv/tftp/
cp /usr/lib/syslinux/modules/bios/chain.c32 /srv/tftp/
cp /usr/lib/syslinux/modules/bios/libcom32.c32 /srv/tftp/
cp /usr/lib/syslinux/modules/bios/libutil.c32 /srv/tftp/
systemctl restart tftpd-hpa
}

setup_apache() {
cp /vagrant/other_conf/apachesite.conf /etc/apache2/sites-enabled/000-default.conf
systemctl restart apache2
}

setup_dhcp() {
sudo cp /vagrant/other_conf/dhcpd.conf /etc/dhcp/dhcpd.conf
sudo cp /vagrant/other_conf/isc-dhcp-server /etc/default/isc-dhcp-server
systemctl restart isc-dhcp-server.service
}

setup_dns() {
sed -i -E "s/^.*\s+kadeploy\s+kadeploy$/10.0.10.100\tkadeploy\tkadeploy/g" /etc/hosts
echo "10.0.10.2 node-1 node-1" >> /etc/hosts
systemctl restart dnsmasq
}

setup_kadeploy() {
mysql <<< "DROP DATABASE IF EXISTS deploy3;\nCREATE DATABASE deploy3;\nCREATE USER IF NOT EXISTS 'deploy'@'localhost';\nGRANT select, insert, update, delete, create, drop, alter, create temporary tables, lock tables ON deploy3.*  TO 'deploy'@'localhost';\nSET PASSWORD FOR  'deploy'@'localhost' = PASSWORD('deploy-password');\nuse deploy3;\nsource /kadeploy/db/db_creation.sql;"
cp /vagrant/keys/id_deploy /home/vagrant/.ssh/deploy_id_rsa
chown vagrant /home/vagrant/.ssh/deploy_id_rsa
chmod og-r /home/vagrant/.ssh/deploy_id_rsa
mkdir -p /var/lib/deploy/bin
sudo ln -fs /vagrant/kalogger /var/lib/deploy/bin/lanpower
sudo ln -fs /vagrant/kalogger /var/lib/deploy/bin/partition
mkdir -p /etc/kadeploy3
sudo ln -fs /vagrant/kadeploy_conf/client.conf /etc/kadeploy3/client.conf
sudo ln -fs /vagrant/kadeploy_conf/clusters.conf /etc/kadeploy3/clusters.conf
sudo ln -fs /vagrant/kadeploy_conf/command.conf /etc/kadeploy3/command.conf
sudo ln -fs /vagrant/kadeploy_conf/sample-cluster.conf /etc/kadeploy3/sample-cluster.conf
sudo ln -fs /vagrant/kadeploy_conf/server.conf /etc/kadeploy3/server.conf
cat  /kadeploy/conf/version <(echo -n vagrant) | tr '\n' '.' > /etc/kadeploy3/version
mkdir -p /var/log/kadeploy3
chown vagrant:vagrant /var/log/kadeploy3
ident2
echo "export KADEPLOY3_LIBS=/kadeploy/lib/" > /home/vagrant/.bashrc
echo "export PATH=$PATH:/kadeploy/bin:/kadeploy/sbin" > /etc/profile.d/kadeploy.sh
}

setup_helper() {
echo "alias resetkdb=\"sudo mysql <<< \\\"DROP DATABASE IF EXISTS deploy3;\nCREATE DATABASE deploy3;\nCREATE USER IF NOT EXISTS 'deploy'@'localhost';\nGRANT select, insert, update, delete, create, drop, alter, create temporary tables, lock tables ON deploy3.*  TO 'deploy'@'localhost';\nSET PASSWORD FOR  'deploy'@'localhost' = PASSWORD('deploy-password');\nuse deploy3;\nsource /kadeploy/db/db_creation.sql;\\\"\"" >> /home/vagrant/.bashrc
}

install_package
setup_tftp
setup_apache
setup_dhcp
setup_dns
setup_kadeploy
setup_helper
