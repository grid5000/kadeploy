#!/bin/bash -e
# MANAGED BY PUPPET
#
# Bootloader (grub) installation script for the kadeploy bootloader step.
# Handle all machines (BIOS, UEFI x86_64&ARM64, Power8/petitboot)
# 1/ Configure grub
# - Configure grub using grub-mkconfig from the deployed env (chroot), if available in
# - Otherwise, write a minimalistic grub.cfg
# 2/ Install grub
# - Install grub using grub-install (and grub-mkimage for UEFI) from the deployed env (chroot), if available in
# - Otherwise, Install grub using grub-install (and grub-mkimage for UEFI) from the deploy kernel
#   (on Power8/petitboot machines, just set the nvram for the petitboot config in both cases)
#
# Options:
# - do nothing if file exists: '/var/lib/kadeploy/bootloader/skip'
# - do not config if file exists: '/var/lib/kadeploy/bootloader/no-config'
# - do not install if file exists: '/var/lib/kadeploy/bootloader/no-install'
# - do not config nor install using grub from the deployed env if file exists: '/var/lib/kadeploy/bootloader/no-grub-from-deployed-env'
# - do not config using grub from the deployed env if file exists: '/var/lib/kadeploy/bootloader/no-grub-mkconfig-from-deployed-env'
# - do not install using grub from the deployed env if file exists: '/var/lib/kadeploy/bootloader/no-grub-install-from-deployed-env'
# - do not use uuid in grub if file exists: '/var/lib/kadeploy/bootloader/no-grub-uuid'
# - do show the grub console if file exists: '/var/lib/kadeploy/bootloader/show-grub-console'
###
## Constants
##
DO_SKIP='/var/lib/kadeploy/bootloader/skip'
DO_NOT_CONFIG='/var/lib/kadeploy/bootloader/no-config'
DO_NOT_INSTALL='/var/lib/kadeploy/bootloader/no-install'
DO_NOT_USE_GRUB_FROM_DEST='/var/lib/kadeploy/bootloader/no-grub-from-deployed-env'
DO_NOT_USE_GRUB_MKCONFIG_FROM_DEST='/var/lib/kadeploy/bootloader/no-grub-mkconfig-from-deployed-env'
DO_NOT_USE_GRUB_INSTALL_FROM_DEST='/var/lib/kadeploy/bootloader/no-grub-install-from-deployed-env'
DO_NOT_USE_UUID='/var/lib/kadeploy/bootloader/no-uuid'
DO_SHOW_MENU='/var/lib/kadeploy/bootloader/show-menu'
DO_DEBUG='/var/lib/kadeploy/bootloader/debug'

###
## Utility functions
##
die() {
  echo "[ERROR] $1" | tee /dev/kmsg 1>&2 && exit 1
}

warn() {
  echo "[WARN] $1" | tee /dev/kmsg 1>&2
}

info() {
  echo "[INFO] $1" | tee /dev/kmsg
}

# Mount system fs in the chroot
set_chroot_sys_fs() {
  # Make the chroot capable of running the grub commands
  grep -q ${KADEPLOY_ENV_EXTRACTION_DIR}/dev /proc/mounts || mount --bind /dev ${KADEPLOY_ENV_EXTRACTION_DIR}/dev
  grep -q ${KADEPLOY_ENV_EXTRACTION_DIR}/sys /proc/mounts || mount --bind /sys ${KADEPLOY_ENV_EXTRACTION_DIR}/sys
  grep -q ${KADEPLOY_ENV_EXTRACTION_DIR}/proc /proc/mounts || mount -t proc none ${KADEPLOY_ENV_EXTRACTION_DIR}/proc
}

# Umount system fs in the chroot
unset_chroot_sys_fs() {
  ! grep -q ${KADEPLOY_ENV_EXTRACTION_DIR}/dev /proc/mounts || umount ${KADEPLOY_ENV_EXTRACTION_DIR}/dev
  ! grep -q ${KADEPLOY_ENV_EXTRACTION_DIR}/sys /proc/mounts || umount ${KADEPLOY_ENV_EXTRACTION_DIR}/sys
  ! grep -q ${KADEPLOY_ENV_EXTRACTION_DIR}/proc /proc/mounts || umount  ${KADEPLOY_ENV_EXTRACTION_DIR}/proc
  # Also umount the EFI partition in case we mounted it
  ! grep -q ${KADEPLOY_ENV_EXTRACTION_DIR}/boot/efi /proc/mounts || umount  ${KADEPLOY_ENV_EXTRACTION_DIR}/boot/efi
}

# get grub name in the deployed environment: on RH-like, it is to be "grub2"
get_dest_grub_name() {
  if [[ "$OS_NAME" =~ centos|redhat ]]; then
    echo 'grub2'
  else
    echo 'grub'
  fi
}

# Check if there is a valid grub available in the deployed environment chroot
is_dest_grub_available() {
  local grub_name=$(get_dest_grub_name)
  [ -e "$GRUB_DEFAULT_FILE" ] || return 1
  if ! chroot ${KADEPLOY_ENV_EXTRACTION_DIR} /bin/true; then
    die "Cannot chroot in the deployed environment: invalid system or wrong architecture?"
  fi
  # Search for the grub commands in the chroot (no need for /sys /dev /proc to be mounted at this time)
  chroot ${KADEPLOY_ENV_EXTRACTION_DIR} which ${grub_name}-mkconfig > /dev/null || return 2
  chroot ${KADEPLOY_ENV_EXTRACTION_DIR} which ${grub_name}-install > /dev/null || return 3
}

# Get inode of file (possibly dereferencing symlink, e.g. for /vmlinux)
get_dest_file_inode() {
  local file=$1
  # Follow symlink
  echo $(chroot $KADEPLOY_ENV_EXTRACTION_DIR stat -L -c "%i" $file)
}

# show dest file, including the eventual symlinked file (eg /vmlinuz -> boot/...)
show_dest_file() {
  local file=$1
  # Follow symlink
  echo $(chroot $KADEPLOY_ENV_EXTRACTION_DIR stat -c "%N" $file)
}

# Compute ord: letter -> number, e.g. a -> 0, b -> 1...
ord() {
  printf "%d" "'$1";
}

# Check that what grub.cfg gives to boot match what the environment description gives (thus, used by kexec)
is_dest_grub_boot_mismatch() {
  local grub_config_file=$1
  info "Sanity-check grub.cfg vs environment description..."
  if [ "$KADEPLOY_OS_KIND" == 'xen' ]; then
    local grub_hypervisor=$(grep multiboot $grub_config_file | grep -o '/[^ ]*xen-[^ ]*' | head -n 1)
    [ -n "$grub_hypervisor" ] || die "Hypervisor not found in grub config file"
    [ -f "$KADEPLOY_ENV_EXTRACTION_DIR/$grub_hypervisor" ] || die "Hypervisor file not found: $grub_hypervisor"
    [ "$(get_dest_file_inode $grub_hypervisor)" == "$(get_dest_file_inode $KADEPLOY_ENV_HYPERVISOR)" ] || \
      die "Hypervisor file mismatch between grub: $(show_dest_file $grub_hypervisor), and the environment descrition $(show_dest_file $KADEPLOY_ENV_HYPERVISOR)"
  fi
  local grub_kernel=$(grep -o "/[^ ]*vmlinu[^ ]*" $grub_config_file | head -n 1)
  [ -n "$grub_kernel" ] || die "Kernel not found in grub config file"
  [ -f "$KADEPLOY_ENV_EXTRACTION_DIR/$grub_kernel" ] || die "Kernel file not found: $grub_kernel"
  [ "$(get_dest_file_inode $grub_kernel)" == "$(get_dest_file_inode $KADEPLOY_ENV_KERNEL)" ] || \
    die "Kernel file mismatch between grub: $(show_dest_file $grub_kernel), and the environment descrition $(show_dest_file $KADEPLOY_ENV_KERNEL)"
  local grub_initrd=$(grep -o "/[^ ]*initr[^ ]*" $grub_config_file | head -n 1)
  [ -n "$grub_initrd" ] || die "Initrd not found in grub config file"
  [ -f "$KADEPLOY_ENV_EXTRACTION_DIR/$grub_initrd" ] || die "Initrd file not found: $grub_initrd"
  [ "$(get_dest_file_inode $grub_initrd)" == "$(get_dest_file_inode $KADEPLOY_ENV_INITRD)" ] || \
    die "Initrd file mismatch between grub: $(show_dest_file $grub_initrd), and the environment descrition $(show_dest_file $KADEPLOY_ENV_INITRD)"
}

###
## Start
##
info "Running bootloader script (install-grub2.sh)..."

[ -e "$DO_DEBUG" ] && set -x

[ -n "$KADEPLOY_OS_KIND" ] || die 'Missing kadeploy variable: KADEPLOY_OS_KIND.'
[ -n "$KADEPLOY_BLOCK_DEVICE" ] || die 'Missing kadeploy variable: KADEPLOY_BLOCK_DEVICE.'
[ -n "$KADEPLOY_ENV_EXTRACTION_DIR" ] || die 'Missing kadeploy variable: KADEPLOY_ENV_EXTRACTION_DIR.'
[ -n "$KADEPLOY_DEPLOY_PART" ] || die 'Missing kadeploy variable: KADEPLOY_DEPLOY_PART.'
[ -n "$KADEPLOY_ENV_KERNEL" ] || die 'Missing kadeploy variable: KADEPLOY_ENV_KERNEL.'
[ -n "$KADEPLOY_ENV_KERNEL_PARAMS" ] || die 'Missing kadeploy variable: KADEPLOY_ENV_KERNEL_PARAMS.'
[ -n "$KADEPLOY_ENV_INITRD" ] || die 'Missing kadeploy variable: KADEPLOY_ENV_INITRD.'
[ "$KADEPLOY_OS_KIND" != 'xen' -o -n "$KADEPLOY_ENV_HYPERVISOR" ] || die 'Missing kadeploy variable: KADEPLOY_ENV_HYPERVISOR.'
[ "$KADEPLOY_OS_KIND" != 'xen' -o -n "$KADEPLOY_ENV_HYPERVISOR_PARAMS" ] || die 'Missing kadeploy variable: KADEPLOY_ENV_HYPERVISOR_PARAMS.'
[ -n "$KADEPLOY_CLUSTER" ] || die 'Missing kadeploy variable: KADEPLOY_CLUSTER.'

# In any case of exit (normal or error), make sure sys, dev, proc are umounted in the dest chroot
trap unset_chroot_sys_fs EXIT

###
## Global variables
##

# Guess the Grub's disk number from the name of the block device ##
# Get the last character of the block device name ("/dev/sdb" -> 'b')
GRUB_DISK_NUM=$(($(ord ${KADEPLOY_BLOCK_DEVICE#${KADEPLOY_BLOCK_DEVICE%?}}) - $(ord 'a')))
# Workaround for pyxis, grub detect the deployed disk as hd1
if [ $KADEPLOY_CLUSTER == "pyxis" ]; then
  GRUB_DISK_NUM=1
fi

CPU_ARCH=$(uname -m)
DEPLOY_PART_UUID=$(lsblk $KADEPLOY_DEPLOY_PART -o UUID -n)

GRUB_CONFIG_FILE="${KADEPLOY_ENV_EXTRACTION_DIR}/boot/grub/grub.cfg"
GRUB_DEFAULT_FILE="${KADEPLOY_ENV_EXTRACTION_DIR}/etc/default/grub"
GRUB_TIMEOUT=0

OS_NAME='unknown'
# It seems that the file os-release is a systemd thing but also present in some
# Linux distributions whitout systemd...
if [ -r "${KADEPLOY_ENV_EXTRACTION_DIR}/etc/os-release" ] && grep -q -e '^ID=' ${KADEPLOY_ENV_EXTRACTION_DIR}/etc/os-release; then
  OS_NAME=$(eval "$(grep -e '^ID=' ${KADEPLOY_ENV_EXTRACTION_DIR}/etc/os-release); echo \$ID")
else
  warn "Could not find out the deployed OS name from /etc/os-release."
fi

[[ -e $KADEPLOY_BLOCK_DEVICE ]] || die "Device not found: $KADEPLOY_BLOCK_DEVICE"
if [ ! -e $DO_SKIP -a ! -e $DO_NOT_CONFIG ]; then
  if [ ! -e $DO_NOT_USE_GRUB_FROM_DEST -a ! -e $DO_NOT_USE_GRUB_MKCONFIG_FROM_DEST ] && is_dest_grub_available; then
    info "Create grub.cfg using grub-mkconfig of the deployed $OS_NAME system..."
    # Update /etc/default/grub file in dest with kadeploy's info
    echo 'GRUB_DISABLE_OS_PROBER=true' >> $GRUB_DEFAULT_FILE
    if [ -e $DO_SHOW_MENU ]; then
      info "Setup grub to show menu on console."
      GRUB_TIMEOUT=3
      echo 'GRUB_TERMINAL=console' >> $GRUB_DEFAULT_FILE
    fi
    if [ -e $DO_NOT_USE_UUID ]; then
      info "Do not use UUID for the root filesystem."
      echo 'GRUB_DISABLE_LINUX_UUID=true' >> $GRUB_DEFAULT_FILE
    else
      echo 'GRUB_DISABLE_LINUX_UUID=false' >> $GRUB_DEFAULT_FILE
    fi
    if [ "$KADEPLOY_OS_KIND" == 'xen' ]; then
      echo "GRUB_CMDLINE_XEN=\"${KADEPLOY_ENV_HYPERVISOR_PARAMS}\"" >> $GRUB_DEFAULT_FILE
      echo 'XEN_OVERRIDE_GRUB_DEFAULT=1' >> $GRUB_DEFAULT_FILE
    fi
    sed -i \
      -e 's/GRUB_DEFAULT=.\+/GRUB_DEFAULT=1/' \
      -e "s/GRUB_TIMEOUT=.\+/GRUB_TIMEOUT=$GRUB_TIMEOUT/" \
      -e 's/GRUB_ENABLE_BLSCFG=.\+/GRUB_ENABLE_BLSCFG=false/' \
      -e "s/GRUB_CMDLINE_LINUX=.\+/GRUB_CMDLINE_LINUX=\"${KADEPLOY_ENV_KERNEL_PARAMS}\"/" \
      $GRUB_DEFAULT_FILE

    # Update the debian/ubuntu debconf, so that updating grub later won't
    # break the config we just set
    if [[ "$OS_NAME" =~ debian|ubuntu ]]; then
      info "Update grub device in the $OS_NAME debconf DB."
      export DEBIAN_FRONTEND=noninteractive
      if echo 'get grub-pc/install_devices' | chroot ${KADEPLOY_ENV_EXTRACTION_DIR} debconf-communicate > /dev/null; then
        echo "set grub-pc/install_devices ${KADEPLOY_DEPLOY_PART}" | chroot ${KADEPLOY_ENV_EXTRACTION_DIR} debconf-communicate > /dev/null || die 'Failed to set debconf grub-pc/install_devices'
      fi
      if echo 'get grub-installer/choose_bootdev' | chroot ${KADEPLOY_ENV_EXTRACTION_DIR} debconf-communicate > /dev/null; then
        echo "set grub-installer/choose_bootdev ${KADEPLOY_DEPLOY_PART}" | chroot ${KADEPLOY_ENV_EXTRACTION_DIR} debconf-communicate > /dev/null || die 'Failed to set debconf grub-installer/choose_bootdev'
      fi
    fi

    # Finally, update the grub configuration
    DEST_GRUB_NAME=$(get_dest_grub_name)
    GRUB_CONFIG_FILE="$KADEPLOY_ENV_EXTRACTION_DIR/boot/$DEST_GRUB_NAME/grub.cfg"
    mkdir -p "$KADEPLOY_ENV_EXTRACTION_DIR/boot/$DEST_GRUB_NAME"
    set_chroot_sys_fs
    # Run sync by ourselves, because otherwise it is run by grub-mkconfig
    # without letting know why the long wait...
    info "Wait for pending IOs to complete (sync)..."
    sync
    info "Run grub-mkconfig..."
    chroot $KADEPLOY_ENV_EXTRACTION_DIR $DEST_GRUB_NAME-mkconfig -o /boot/$DEST_GRUB_NAME/grub.cfg >& /dev/kmsg

    # And check that the kernel, initrd and hypervisor grub will boot match the ones of the environment description
    if [ "$KADEPLOY_OS_KIND" == 'xen' ]; then
      # The xen menuentry is not the 1st one. Remove everything before it
      sed -e "1,/menuentry '.\+, with Xen hypervisor'/d" $GRUB_CONFIG_FILE >/tmp/grub.cfg.truncated
      if [ -s /tmp/grub.cfg.truncated ]; then
        is_dest_grub_boot_mismatch /tmp/grub.cfg.truncated
      else
        warn "Could not verify if the grub boot will match the environment description. Continuing anyway..."
      fi
    else
      is_dest_grub_boot_mismatch $GRUB_CONFIG_FILE
    fi
  else
    # Not using grub-mkconfig from dest
    # Set a very basic grub configuration
    info "Configure a minimalistic $GRUB_CONFIG_FILE for $KADEPLOY_OS_KIND..."
    mkdir -p "$KADEPLOY_ENV_EXTRACTION_DIR/boot/grub"
    if [ -e $DO_SHOW_MENU ]; then
      info "Setup grub to show menu on console."
      cat <<EOF > $GRUB_CONFIG_FILE
terminal_input console
terminal_output console
EOF
      GRUB_TIMEOUT=3
    fi
    cat <<EOF > $GRUB_CONFIG_FILE
set default=1
set timeout=$GRUB_TIMEOUT

menuentry "${KADEPLOY_OS_KIND}" {
  set root=(hd${GRUB_DISK_NUM},${KADEPLOY_DEPLOY_PART_NUM})
EOF
    if [ $CPU_ARCH == 'ppc64le' ]; then
        cat <<EOF >> $GRUB_CONFIG_FILE
  search --no-floppy --fs-uuid --set=root ${DEPLOY_PART_UUID}
EOF
    fi
    case $KADEPLOY_OS_KIND in
      'linux')
        if [ -e $DO_NOT_USE_UUID ]; then
          info "Do not use UUID for the root filesystem."
          cat <<EOF >> $GRUB_CONFIG_FILE
  linux $KADEPLOY_ENV_KERNEL $KADEPLOY_ENV_KERNEL_PARAMS root=$KADEPLOY_DEPLOY_PART ro
EOF
        else
          cat <<EOF >> $GRUB_CONFIG_FILE
  linux $KADEPLOY_ENV_KERNEL $KADEPLOY_ENV_KERNEL_PARAMS root=UUID=$DEPLOY_PART_UUID ro
EOF
        fi
        [ -n "$KADEPLOY_ENV_INITRD" ] && cat <<EOF >> $GRUB_CONFIG_FILE
  initrd $KADEPLOY_ENV_INITRD
EOF
        ;;
      'xen')
        cat <<EOF >> $GRUB_CONFIG_FILE
  multiboot $KADEPLOY_ENV_HYPERVISOR $KADEPLOY_ENV_HYPERVISOR_PARAMS
EOF
        if [ -e $DO_NOT_USE_UUID ]; then
          cat <<EOF >> $GRUB_CONFIG_FILE
  module $KADEPLOY_ENV_KERNEL $KADEPLOY_ENV_KERNEL_PARAMS root=$KADEPLOY_DEPLOY_PART ro
EOF
        else
          cat <<EOF >> $GRUB_CONFIG_FILE
  module $KADEPLOY_ENV_KERNEL $KADEPLOY_ENV_KERNEL_PARAMS root=UUID=$DEPLOY_PART_UUID ro
EOF
        fi
        [ -n "$KADEPLOY_ENV_INITRD" ] && cat <<EOF >> $GRUB_CONFIG_FILE
  module $KADEPLOY_ENV_INITRD
EOF
        ;;
      'bsd')
        cat <<EOF >> $GRUB_CONFIG_FILE
  insmod ufs1
  insmod ufs2
  insmod zfs
  chainloader +1
EOF
        ;;
      'windows')
        cat <<EOF >> $GRUB_CONFIG_FILE
  insmod fat
  insmod ntfs
  ntldr /bootmgr
EOF
        ;;
      *)
        die "Unknown operating system '${KADEPLOY_OS_KIND}'" 1>&2
      ;;
    esac
    cat <<EOF >> $GRUB_CONFIG_FILE
}
EOF
  fi
else
  info "No grub configuration done, as requested."
fi

if [ ! -e "$DO_SKIP" -a ! -e "$DO_NOT_INSTALL" ]; then
  CHROOT_DIR=""
  GRUB_NAME="grub"
  if [ ! -e "$DO_NOT_USE_GRUB_FROM_DEST" -a ! -e "$DO_NOT_USE_GRUB_INSTALL_FROM_DEST" ] && is_dest_grub_available; then
    info "Installing grub using grub-install from the deployed system..."
    set_chroot_sys_fs
    CHROOT_DIR=$KADEPLOY_ENV_EXTRACTION_DIR
    GRUB_NAME=$(get_dest_grub_name)
  else
    info "Installing grub using grub-install from the kadeploy kernel..."
  fi
  if [ $CPU_ARCH == 'ppc64le' ]; then
    # Power systems using petitboot do not need a grub installation.
    # NVRAM must be set to boot the correct partition however.
    info "Power system using petitboot: do not run grub-install."
    [ -e /dev/disk/by-uuid/$DEPLOY_PART_UUID ] || dir "Deploy part with UUID=$DEPLOY_PART_UUID not found."
    info "Set NVRAM to let petitboot boot part with UUID=$DEPLOY_PART_UUID"
    nvram -p common --update-config "petitboot,bootdevs=network uuid:$DEPLOY_PART_UUID"
    nvram -p common --update-config 'petitboot,timeout=15'
  else
    if [ -d '/sys/firmware/efi' ]; then
      # This is a UEFI system
      if [ $CPU_ARCH == 'x86_64' ]; then
        GRUB_TARGET='x86_64-efi'
      elif [ $CPU_ARCH == 'aarch64' ]; then
        GRUB_TARGET='arm64-efi'
      else
        die "Unable to install grub2 UEFI, unsupported CPU architecture: '$CPU_ARCH'"
      fi
      if [ -n "$CHROOT_DIR" ]; then
        # From within the chroot
        # Command to create the grub UEFI boot image
        GRUB_MKIMAGE_CMD="${GRUB_NAME}-mkimage -o /boot/efi/EFI/part$KADEPLOY_DEPLOY_PART_NUM/boot.efi --format=$GRUB_TARGET --prefix=(hd$GRUB_DISK_NUM,gpt$KADEPLOY_DEPLOY_PART_NUM)/boot/$GRUB_NAME ext2 part_gpt"
        # Command to install the grub UEFI modules in the EFI partition
        GRUB_INSTALL_CMD="${GRUB_NAME}-install --target=$GRUB_TARGET --efi-directory=/boot/efi --bootloader-id=part$KADEPLOY_DEPLOY_PART_NUM --boot-directory=/boot"
      else
        # From outside the chroot
        # Command to create the grub UEFI boot image
        GRUB_MKIMAGE_CMD="${GRUB_NAME}-mkimage -o $KADEPLOY_ENV_EXTRACTION_DIR/boot/efi/EFI/part$KADEPLOY_DEPLOY_PART_NUM/boot.efi --format=$GRUB_TARGET --prefix=(hd$GRUB_DISK_NUM,gpt$KADEPLOY_DEPLOY_PART_NUM)/boot/$GRUB_NAME ext2 part_gpt"
        # Command to install the grub UEFI modules in the EFI partition
        GRUB_INSTALL_CMD="${GRUB_NAME}-install --target=$GRUB_TARGET --efi-directory=$KADEPLOY_ENV_EXTRACTION_DIR/boot/efi --bootloader-id=part$KADEPLOY_DEPLOY_PART_NUM --boot-directory=$KADEPLOY_ENV_EXTRACTION_DIR/boot"
      fi
      mkdir -p "$KADEPLOY_ENV_EXTRACTION_DIR/boot/efi"
      grep -q ${KADEPLOY_ENV_EXTRACTION_DIR}/boot/efi /proc/mounts || mount ${KADEPLOY_BLOCK_DEVICE}4 ${KADEPLOY_ENV_EXTRACTION_DIR}/boot/efi
      # Run grub-mkimage, in the chroot if CHROOT_DIR is set
      info "UEFI $CPU_ARCH system, create grub efi image with grub-mkimage..."
      ${CHROOT_DIR:+chroot $CHROOT_DIR} $GRUB_MKIMAGE_CMD
    else
      # Command for the BIOS legacy grub installation
      if [ -n "$CHROOT_DIR" ]; then
        GRUB_INSTALL_CMD="${GRUB_NAME}-install --no-floppy --force ${KADEPLOY_DEPLOY_PART}"
      else
        GRUB_INSTALL_CMD="${GRUB_NAME}-install --no-floppy --root-directory=$KADEPLOY_ENV_EXTRACTION_DIR --force ${KADEPLOY_DEPLOY_PART}"
      fi
    fi
    # Run grub-install, in the chroot if CHROOT_DIR is set
    info "Run grub-install..."
    ${CHROOT_DIR:+chroot $CHROOT_DIR} $GRUB_INSTALL_CMD
  fi
else
  info "No grub installation done, as requested."
fi



if chroot ${KADEPLOY_ENV_EXTRACTION_DIR} /bin/ssh-keygen -A; then
   echo "it just works"
fi


###
## End
##

info "End bootloader script."
