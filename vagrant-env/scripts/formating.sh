#!/bin/bash -e

if [ "$KADEPLOY_FORMAT_PART" = "DEPLOY" ]; then
    echo "Kadeploy start deploy partition formating ($KADEPLOY_DEPLOY_PART in $KADEPLOY_FS_TYPE)" > /dev/kmsg
    mkdir -p "$KADEPLOY_ENV_EXTRACTION_DIR"
    umount "$KADEPLOY_DEPLOY_PART" 2>/dev/null || true
    mkfs -t "$KADEPLOY_FS_TYPE" -F -b 4096 -O sparse_super,filetype,resize_inode,dir_index -q "$KADEPLOY_DEPLOY_PART"
    echo "Deploy partition formating done" > /dev/kmsg
elif [ "$KADEPLOY_FORMAT_PART" = "TMP" ]; then
    echo "Kadeploy start tmp partition formating ($KADEPLOY_TMP_PART in $KADEPLOY_FS_TYPE_TMP)" > /dev/kmsg
    sleep 50
    umount "$KADEPLOY_TMP_PART" 2>/dev/null || true
    if [ -b "$KADEPLOY_TMP_PART" ] && [[ -n $(lsblk "$KADEPLOY_TMP_PART" -o UUID -n) ]]; then
        mkfs -t "$KADEPLOY_FS_TYPE_TMP" -U $(lsblk "$KADEPLOY_TMP_PART" -o UUID -n) -F -b 4096 -O sparse_super,filetype,resize_inode,dir_index -q "$KADEPLOY_TMP_PART"
    else
        mkfs -t "$KADEPLOY_FS_TYPE_TMP" -F -b 4096 -O sparse_super,filetype,resize_inode,dir_index -q "$KADEPLOY_TMP_PART"
    fi
    mount "$KADEPLOY_TMP_PART" /mnt/tmp
    chmod 1777 /mnt/tmp
    umount /mnt/tmp
    echo "Tmp partition formating done" > /dev/kmsg
elif [ "$KADEPLOY_FORMAT_PART" = "SWAP" ]; then
    SWAP_PART="${KADEPLOY_SWAP_PART}"
    echo "Kadeploy start swap partition formating ($KADEPLOY_SWAP_PART)" > /dev/kmsg
    # preserve UUID for swap partition since it's shared between prod & deploy
    if [ -b "$KADEPLOY_SWAP_PART" ] && [[ -n $(lsblk "$KADEPLOY_SWAP_PART" -o UUID -n) ]]; then
        mkswap --uuid $(lsblk "$KADEPLOY_SWAP_PART" -o UUID -n) "$KADEPLOY_SWAP_PART"
    else
        mkswap "$KADEPLOY_SWAP_PART"
    fi
    echo "Swap partition formating done" > /dev/kmsg
fi
