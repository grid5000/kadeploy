#!/bin/bash -e

if [ $KADEPLOY_KEXEC_KIND = "to_deployed_env" ]; then
    kernel=${KADEPLOY_ENV_EXTRACTION_DIR}${KADEPLOY_ENV_KERNEL}
    initrd=${KADEPLOY_ENV_EXTRACTION_DIR}${KADEPLOY_ENV_INITRD}
    /sbin/kexec -l $kernel --initrd=$initrd --append="root=/dev/disk/by-partlabel/${KADEPLOY_DEPLOY_LABEL} ${KADEPLOY_ENV_KERNEL_PARAMS}"
elif [ $KADEPLOY_KEXEC_KIND = "to_deploy_kernel" ]; then
    kernel=${KADEPLOY_ENV_KEXEC_REPOSITORY}/kadeploy3-deploy-kernel-bookworm.vmlinuz
    initrd=${KADEPLOY_ENV_KEXEC_REPOSITORY}/kadeploy3-deploy-kernel-bookworm.initrd.img
    /sbin/kexec -l $kernel --initrd=$initrd --append="${KADEPLOY_DEPLOY_ENV_KERNEL_PARAMS}"
fi
systemctl kexec

