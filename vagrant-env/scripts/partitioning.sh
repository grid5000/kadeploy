#!/bin/bash

PARTED_OPTS="--script -a optimal"
UNIT="GB"
env > /tmp/envatexecute
# Set the filesystem type of the deployment partition
# (in order for parted to write the corresponding MSDOS partition type
# in the partition table)
if [ $KADEPLOY_DEPLOY_PART_NUM -ne "5" ]; then
  FSTYPE[$KADEPLOY_DEPLOY_PART_NUM]=$KADEPLOY_FS_TYPE
  FSTYPE[5]="ext4"
else
  FSTYPE[$KADEPLOY_DEPLOY_PART_NUM]=$KADEPLOY_FS_TYPE
fi

function do_parted()
{
  echo /sbin/parted $PARTED_OPTS $KADEPLOY_BLOCK_DEVICE unit $UNIT $@ >> /dev/kmsg
  /sbin/parted $PARTED_OPTS $KADEPLOY_BLOCK_DEVICE unit $UNIT $@
}

echo "Kadeploy starting partitionning disk $KADEPLOY_BLOCK_DEVICE..." > /dev/kmsg

until stat $KADEPLOY_BLOCK_DEVICE
do
  sleep 2
done

do_parted "mklabel gpt"
do_parted "mkpart KDPL_SWAP_$KADEPLOY_DISK_NAME linux-swap 0% 1%"
do_parted "mkpart KDPL_PROD_$KADEPLOY_DISK_NAME ${FSTYPE[2]} 1% 2"
do_parted "mkpart KDPL_DEPLOY_$KADEPLOY_DISK_NAME ${FSTYPE[3]} 2 4"
do_parted "mkpart efi 4 5"
do_parted "toggle 4 boot"
do_parted "mkpart KDPL_TMP_$KADEPLOY_DISK_NAME ${FSTYPE[5]} 5 100%"

# https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=5124
#do_parted "toggle $KADEPLOY_DEPLOY_PART_NUM boot"
do_parted "align-check optimal 1"
do_parted "align-check optimal 2"
do_parted "align-check optimal 3"
do_parted "align-check optimal 4"
do_parted "align-check optimal 5"

/sbin/partprobe $KADEPLOY_BLOCK_DEVICE

if [ -d /sys/firmware/efi ];
then
  until stat ${KADEPLOY_BLOCK_DEVICE}4
  do
    sleep 1
  done
  if ! $(/sbin/parted $PARTED_OPTS -m ${KADEPLOY_BLOCK_DEVICE}4 print | grep -q fat32); then
      mkdosfs -F 32 ${KADEPLOY_BLOCK_DEVICE}4
  fi
fi
