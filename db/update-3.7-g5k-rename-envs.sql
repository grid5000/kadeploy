--
-- For public env, rename the env and put the old name as alias
-- eg: debian11-ppc64-min => debian11-min
UPDATE  `environments`  SET `alias` = `name` WHERE `visibility` = "public" AND name LIKE "%-x64-%";
UPDATE  `environments`  SET `alias` = `name` WHERE `visibility` = "public" AND name LIKE "%-ppc64-%";
UPDATE  `environments`  SET `alias` = `name` WHERE `visibility` = "public" AND name LIKE "%-arm64-%";

UPDATE  `environments`  SET `name` = REPLACE(`name`,'-x64','') WHERE `visibility` = "public" AND name LIKE "%-x64-%";
UPDATE  `environments`  SET `name` = REPLACE(`name`,'-ppc64','') WHERE `visibility` = "public" AND name LIKE "%-ppc64-%";
UPDATE  `environments`  SET `name` = REPLACE(`name`,'-arch64','') WHERE `visibility` = "public" AND name LIKE "%-arm64-%";

