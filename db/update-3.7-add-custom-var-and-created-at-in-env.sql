--
-- New field in the table `environments`
--
ALTER TABLE `environments` ADD `custom_variables` TEXT NOT NULL default '';
ALTER TABLE `environments` ADD `created_at` DATETIME NULL default NULL;
