--
-- New field in the table `environments`
--
ALTER TABLE `environments` ADD `arch` varchar(56) NOT NULL;
ALTER TABLE `environments` ADD `alias` varchar(255) NOT NULL default '';

-- Larger env name in log table
ALTER TABLE `log` MODIFY `env` varchar(255) NOT NULL;

-- add architectures to env based on name
UPDATE `environments` SET `arch` = "ppc64le"  WHERE `name` LIKE "%ppc64%";
UPDATE `environments` SET `arch` = "aarch64"  WHERE `name` LIKE "%arm64%";
UPDATE `environments` SET `arch` = "x86_64"  WHERE `arch` = "";
